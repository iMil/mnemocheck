# mnemocheck

This `Dockerfile` is aimed at checking that a mnemonic word list (seed phrase) matches a _Bitcoin_, _Litecoin_, _Dogecoin_, _Dash_, _Ethereum_ or _XRP_ address, for example to ensure your seed backup is actually correct without leaking any information to the network or local filesystem.

* DON'T use a binary version of this `Dockerfile`, build it!
* DON'T `echo` or write your seed to a file , use some kind of piped encryption command like [password-store](https://www.passwordstore.org/)
* DON'T run the resulting container with network enabled!
* DON'T run this on an untrusted machine!

`mnemocheck` uses [bip-utils](https://pypi.org/project/bip-utils/) to derive the public key from the seed words.

## Build

```sh
$ docker build . -t mnemocheck
```

## Usage

_Pipe_ the mnemonic to the container, specifying which currency address you are checking (defaults to `btc`).

Available currencies parameters:

* `btc`
* `ltc`
* `doge`
* `dash`
* `eth`
* `xrp`

Example:
```sh
$ gpg -d my_wallet_seed.asc|docker run --network none -i mnemocheck eth
eth address: 0x6154ddbf4c3B43F84253460B83547e87E35FE7ba
```
