#!/usr/bin/env python

from bip_utils import Bip39MnemonicValidator, Bip39SeedGenerator, Bip44, Bip44Coins, Bip44Changes
import sys

if len(sys.argv) < 2:
    print("missing currency")
    sys.exit(1)

cur = sys.argv[1]

printpriv = False

if len(sys.argv) > 2:
    printpriv = True

curlist = {
    'btc': Bip44Coins.BITCOIN,
    'eth': Bip44Coins.ETHEREUM,
    'xrp': Bip44Coins.RIPPLE,
    'ltc': Bip44Coins.LITECOIN,
    'doge': Bip44Coins.DOGECOIN,
    'dash': Bip44Coins.DASH,

}

with sys.stdin as f:
    mnemonic = ' '.join(f.read().split())

if not Bip39MnemonicValidator(mnemonic).Validate():
    print("invalid mnemonic")
    sys.exit(1)

seed_bytes = Bip39SeedGenerator(mnemonic).Generate("")
bip44_mst = Bip44.FromSeed(seed_bytes, curlist[cur])
bip44_acc = bip44_mst.Purpose() \
                     .Coin()    \
                     .Account(0)
bip44_change = bip44_acc.Change(Bip44Changes.CHAIN_EXT)
bip44_addr = bip44_change.AddressIndex(0)

if printpriv is True:
    print("{0} private key: {1}".format(
        cur, bip44_addr.PrivateKey().Raw().ToHex())
    )

print("{0} address: {1}".format(cur, bip44_addr.PublicKey().ToAddress()))
