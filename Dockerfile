FROM python:3.8-slim

RUN apt-get update && \
    apt-get install --no-install-recommends -y build-essential && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir bip-utils

COPY m2k.py /

ENTRYPOINT ["/m2k.py"]
CMD ["btc"]
